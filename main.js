const fs = require('fs')

// Function to check if a word exists in a grid.
// starting after the first match in the grid.
// index: The position of the word that you are looking for.
// x, y: current position in 2D array. 
// direction: cardinal direction that you are searching in [N, NE, E, SE, S, SW, W, NW].

function checkMatch(grid, word, x, y, size, index, direction) {

    // Word found
    if (index == word.length){
        return true;
    }

    // Out of Boundary 
    if (x < 0 || y < 0 || x >= size[0] || y >= size[1]){
        return false; 
    }

    // If grid matches with a letter while 
    // recursion 
    if (grid[x][y] == word[index]) {
        //continue recursion in the same direction.
        switch(direction) {
            case "N":
                return checkMatch(grid, word, x-1, y, size, index+1, direction);
            case "NE": 
                return checkMatch(grid, word, x-1, y+1, size, index+1, direction);
            case "E": 
                return checkMatch(grid, word, x, y+1, size, index+1, direction);
            case "SE": 
                return checkMatch(grid, word, x+1, y+1, size, index+1, direction);
            case "S":
                return checkMatch(grid, word, x+1, y, size, index+1, direction);
            case "SW":
                return checkMatch(grid, word, x+1, y-1, size, index+1, direction); 
            case "W":
                return checkMatch(grid, word, x, y-1, size, index+1, direction);
            case "NW":
                return checkMatch(grid, word, x-1, y-1, size, index+1, direction);
            default:
                break;
        }
    } else {
        // No match found
        return false;
    }
}

// Function to check if the word exists in the grid or not and acts as a wrapper for recursive function 
function searchGrid(grid, word, size){
    let len = word.length;

    // Checks if word can even fix in the grid.
    if (len > size[0] || len > size[1]) {
        console.log(word, " is too big!");
        return;
    }
    // Makes calculating endpoint easier.
    len--;

    // Traverse in the grid 
    for (let x = 0; x < size[0]; x++) { 
        for (let y = 0; y < size[1]; y++) {

            //if first letter matches, then check for rest of word in each direction using a recursive function 
            if(grid[x][y] == word[0]) {
                if(checkMatch(grid, word, x-1, y, size, 1, "N")){
                    console.log(word," ",x,":",y," ", x-len,":", y);
                }
                if(checkMatch(grid, word, x-1, y+1, size, 1, "NE")){
                    console.log(word," ",x,":",y," ", x-len,":", y+len);
                }
                if(checkMatch(grid, word, x, y+1, size, 1, "E")){
                    console.log(word," ",x,":",y," ", x,":", y+len);
                }
                if(checkMatch(grid, word, x+1, y+1, size, 1, "SE")){
                    console.log(word," ",x,":",y," ", x+len,":", y+len);
                }
                if(checkMatch(grid, word, x+1, y, size, 1, "S")){
                    console.log(word," ",x,":",y," ", x+len,":", y);
                }
                if(checkMatch(grid, word, x+1, y-1, size, 1, "SW")){
                    console.log(word," ",x,":",y," ", x+len,":", y-len);
                }
                if(checkMatch(grid, word, x, y-1, size, 1, "W")){
                    console.log(word," ",x,":",y," ", x,":", y-len);
                }
                if(checkMatch(grid, word, x-1, y-1, size, 1, "NW")){
                    console.log(word," ",x,":",y," ", x-len,":", y-len);
                }
            }
        }
    }
    return false;
}

const data = fs.readFileSync('./data.txt', 'utf-8').split('\n').map( x => x.trim());
var size = data[0].split('x').map( x => parseInt(x));
var puzzle = data.slice(1, size[0] + 1).map( x => x.split(' '));
var words = data.slice(size[0] + 1);

words.map(x => searchGrid(puzzle, x, size));
